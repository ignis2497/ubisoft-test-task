# Ubisoft test task (Server)
Test task for Ubisoft

## Using
- Python 3.7
- Mongo DB
- AioHTTP

## Installation

It's recommended to use virtual environment

- Install Mongo DB:
    https://docs.mongodb.com/manual/installation/#mongodb-community-edition

- Install dependencies:
    ```
    pip install -r requirements.txt
    ```

## Usage
For starting the server `runserver.py` can be used:
```
python runserver.py [-h] [--regenerate_db] [HOST:PORT]
```
E.g.
```
python runserver.py 127.0.0.1:8000 --regenerate_db
```
You can use `python runserver.py -h` to get more info.

## Testing
For testing purposes can be used those scripts:

- `tasks_creator.py` - can be used to simulate players that creates tasks randomly
- `tasks_viewer.py` - can be used to simulate client that retrieves tasks data from server
- `load_test.py` - _(not recommended)_ can be used to simulate each player separately. Each player can create tasks randomly and retrieve tasks info about other random players in it's field of view