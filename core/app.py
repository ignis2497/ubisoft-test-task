import importlib

from aiohttp import web

from core import settings
from core import LOOP

APP = web.Application(loop=LOOP, debug=settings.DEBUG)


def collect_routes():
    for app in settings.INSTALLED_APPS:
        module = app + '.routes'
        imported = importlib.import_module(module)

        for path in imported.paths:
            yield path


APP.add_routes(collect_routes())
