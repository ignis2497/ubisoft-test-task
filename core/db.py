import asyncio

import motor.motor_asyncio as aiomotor

from core import settings, LOOP
from core.app import APP


async def init_mongo(loop):
    mongo_uri = "mongodb://{}:{}".format(settings.DATABASE['host'], settings.DATABASE['port'])
    conn = aiomotor.AsyncIOMotorClient(
        mongo_uri,
        maxPoolSize=settings.DATABASE['max_pool_size'],
        io_loop=loop
    )
    db_name = settings.DATABASE['database']
    return conn[db_name]


async def setup_mongo(app, loop):
    mongo = await init_mongo(loop)

    async def close_mongo(app):
        mongo.client.close()

    app.on_cleanup.append(close_mongo)
    return mongo


DB = LOOP.run_until_complete(setup_mongo(APP, LOOP))
