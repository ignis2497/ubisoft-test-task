DEBUG = False

INSTALLED_APPS = (
    'players',
)

DATABASE = {
    "host": "127.0.0.1",
    "port": 27017,
    "database": "ubi_test",
    "max_pool_size": 1,
}


MAX_PLAYER_TASKS = 4

FAKE_PLAYERS_AMOUNT = 20000

try:
    from .local_settings import *
except ImportError:
    pass
