from motor.frameworks import asyncio

from core import settings

LOOP = asyncio.get_event_loop()
LOOP.set_debug(settings.DEBUG)
