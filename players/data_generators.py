from datetime import datetime, timedelta

from faker import Faker

from core import settings
from core.db import DB

FAKE = Faker()


def get_fake_player():
    return {'name': FAKE.name()}


def fake_players(amount):
    for _ in range(amount):
        yield get_fake_player()


def get_fake_task(player_id):
    dt_now = datetime.now()
    task_time = FAKE.random.uniform(10, 10 * 60)
    start_time = FAKE.date_time_between(dt_now - timedelta(seconds=task_time), dt_now)

    return {
        'player_id': player_id,
        'task_time': task_time,
        'started_at': start_time,
        'end_time': start_time + timedelta(seconds=task_time)
    }


def get_fake_tasks(player_ids):
    for player_id in player_ids:
        for _ in range(FAKE.random.randint(1, 4)):
            yield get_fake_task(player_id)


async def has_data():
    """Checks is database already has data"""
    collection_names = await DB.list_collection_names()

    if 'players' not in collection_names:
        return False
    if await DB['players'].count_documents({}) == 0:
        return False

    return True


async def prepare_fake_data():
    if await has_data():
        return

    print('Populating DB with fake data...')

    players_result = await DB['players'].insert_many(fake_players(settings.FAKE_PLAYERS_AMOUNT))
    tasks_result = await DB['tasks'].insert_many(get_fake_tasks(players_result.inserted_ids))

    print(f'Created {len(players_result.inserted_ids)} players and {len(tasks_result.inserted_ids)} tasks for them')


async def clear_db():
    await DB.drop_collection('players')
    await DB.drop_collection('tasks')
