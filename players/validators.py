import trafaret as t
from trafaret.contrib.object_id import MongoId
from trafaret.contrib.rfc_3339 import DateTime


player = t.Dict({
    t.Key('_id', optional=True): MongoId(),
    t.Key('name'): t.String(max_length=50),
})


task = t.Dict({
    t.Key('_id', optional=True): MongoId(),
    t.Key('player_id'): MongoId(),
    t.Key('task_time'): t.Float(gte=10, lte=10 * 60),
    t.Key('started_at'): DateTime(),
    t.Key('end_time'): DateTime(),
})
