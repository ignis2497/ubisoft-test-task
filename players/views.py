from dataclasses import asdict
from datetime import datetime, timedelta

import time
from aiohttp import web
from bson import ObjectId, json_util

from core import settings
from core.db import DB
from players.data import Player, Task


async def players_list(request):
    amount = await DB['players'].count_documents({})
    players = await DB['players'].find().to_list(amount)

    return web.Response(text=json_util.dumps(players))


async def player_tasks(request):
    """View to retrieve and return user's tasks"""
    player_id = request.match_info['id']
    db_player = await DB['players'].find_one({'_id': ObjectId(player_id)})

    if db_player is None:
        raise web.HTTPNotFound()

    player = Player(**db_player)

    tasks = []
    async for task in player.tasks:
        tasks.append({
            '_id': task.db_id,
            'time_left': task.time_left,
        })

    return web.Response(text=json_util.dumps(tasks))


async def bulk_players_tasks(request):
    """View to retrieve and return many users tasks"""
    data = await request.json()
    players_ids = data['players']
    db_players = DB['players'].find({'_id': {'$in': [ObjectId(player_id) for player_id in players_ids]}})

    players = {}
    async for db_player in db_players:
        player = Player(**db_player)

        tasks = []
        async for task in player.tasks:
            tasks.append({
                '_id': task.db_id,
                'time_left': task.time_left,
            })

        players[str(player.db_id)] = tasks

    return web.Response(text=json_util.dumps(players))


async def create_task(request):
    player_id = request.match_info['id']
    db_player = await DB['players'].find_one({'_id': ObjectId(player_id)})

    if db_player is None:
        raise web.HTTPNotFound()

    if await Player(**db_player).count_active_tasks() >= settings.MAX_PLAYER_TASKS:
        raise web.HTTPBadRequest(reason='Max amount of tasks reached')

    task_data = await request.json()
    dt_now = datetime.now()
    task = await Task.create(
        player_id=player_id,
        started_at=dt_now,
        task_time=task_data['task_time'],
        end_time=dt_now + timedelta(seconds=task_data['task_time']),
    )

    return web.Response(text=json_util.dumps(asdict(task)), status=201)
