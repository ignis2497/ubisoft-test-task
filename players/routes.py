from aiohttp import web

from players.views import player_tasks, create_task, players_list, bulk_players_tasks

paths = (
    web.get('/players/', players_list),
    web.get('/player/{id}/', player_tasks),
    web.post('/players/tasks/', bulk_players_tasks),
    web.post('/player/{id}/create_task/', create_task),
)
