from dataclasses import dataclass
from datetime import datetime

from bson import ObjectId

from core.db import DB
from players.validators import player as player_trafaret, task as task_trafaret


@dataclass
class BaseData:
    _id: ObjectId

    class Meta:
        validator = None
        collection_name = None

    @classmethod
    async def validate(cls, data):
        return cls.Meta.validator.check(data)

    @classmethod
    async def create(cls, **kwargs):
        data = await cls.validate(kwargs)
        await cls.get_collection().insert_one(data)

        return cls(**data)

    @classmethod
    def get_collection(cls):
        name = cls.Meta.collection_name
        if name is None:
            name = cls.__name__.lower() + 's'

        return DB[name]

    @property
    def db_id(self):
        return self._id


@dataclass
class Player(BaseData):
    name: str

    class Meta(BaseData.Meta):
        validator = player_trafaret

    def __str__(self):
        return f'{self.name}({self._id})'

    @property
    async def tasks(self):
        await Task.get_collection().delete_many({
            'player_id': self._id,
            'end_time': {
                '$lte': datetime.now(),
            }
        })

        async for db_task in Task.get_collection().find({'player_id': self._id}):
            task = Task(**db_task)
            yield task

    async def count_active_tasks(self):
        return await Task.get_collection().count_documents({
            'player_id': self._id,
            'end_time': {
                '$gt': datetime.now(),
            }
        })


@dataclass
class Task(BaseData):
    player_id: ObjectId
    task_time: float
    started_at: datetime
    end_time: datetime

    class Meta(BaseData.Meta):
        validator = task_trafaret

    def __str__(self):
        return f'{self._id}, {self.time_left}'

    @property
    def time_left(self):
        return (self.end_time - datetime.now()).total_seconds()
