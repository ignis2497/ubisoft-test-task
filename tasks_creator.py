import random
import time
from multiprocessing import Process

import requests

BASE_URL = 'http://127.0.0.1:8000'


class Player(object):
    def __init__(self, id_, name):
        self.id = id_
        self.name = name

    def create_task(self):
        task_data = {
            'task_time': random.uniform(10, 10 * 60)
        }
        response = requests.post(f'{BASE_URL}/player/{self.id}/create_task/', json=task_data)

        if response.status_code != 201:
            return None

        return response.json()


class Creator(object):
    DELAY = (1, 3)
    PLAYERS_AMOUNT = (200, 2000)

    def __init__(self, players):
        self.players = players

    def creator_loop(self):
        loop_count = 0
        while True:
            loop_count += 1

            print(f'Loop #{loop_count} started')

            t_start = time.time()
            players_to_start = random.choices(self.players, k=random.randint(*Creator.PLAYERS_AMOUNT))

            for player in players_to_start:
                for i in range(random.randint(1, 4)):
                    created_task = player.create_task()
                    if created_task:
                        print(f'    {player.name} created task for {created_task["task_time"]}s')

            print(f'===  Tasks created in {time.time() - t_start}s')

            time.sleep(random.randint(*self.DELAY))


def create_players():
    while True:
        try:
            response = requests.get(f'{BASE_URL}/players/')
            break
        except requests.ConnectionError:
            print('Waiting for server...')
            time.sleep(5)

    players_data = response.json()

    players = []
    for player_data in players_data:
        players.append(Player(id_=player_data["_id"]["$oid"], name=player_data['name']))

    return players


if __name__ == '__main__':
    players = create_players()
    creator = Creator(players)

    creator.creator_loop()
