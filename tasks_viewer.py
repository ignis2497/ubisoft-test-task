import random
import time
from multiprocessing import Process

import requests

BASE_URL = 'http://127.0.0.1:8000'


class Player(object):
    def __init__(self, id_, name):
        self.id = id_
        self.name = name

    def get_tasks(self):
        response = requests.get(f'{BASE_URL}/player/{self.id}/')
        response.raise_for_status()

        return response.json()


class Viewer(object):
    FOV = (32, 32)
    DELAY = (0, 5)

    def __init__(self, players):
        self.players = players

    @staticmethod
    def tasks_to_text(tasks):
        return '; '.join(f'{task["_id"]["$oid"]}: {task["time_left"]}s' for task in tasks)

    def viewer_loop(self):
        loop_count = 0
        while True:
            loop_count += 1

            print(f'Loop #{loop_count} started')

            t_start = time.time()

            players_in_view = random.choices(self.players, k=random.randint(0, self.FOV[0] * self.FOV[1]))
            for player in players_in_view:
                tasks = player.get_tasks()
                print(f'    {player.name} tasks: {tasks}')

            print(f'===  Tasks retrieved in {time.time() - t_start}s')

            time.sleep(random.randint(*self.DELAY))


class ViewerBulk(object):
    FOV = (32, 32)
    DELAY = (0, 5)

    def __init__(self, players):
        self.players = players

    def get_tasks_bulk(self, players):
        response = requests.post(f'{BASE_URL}/players/tasks/', json={'players': [p.id for p in players]})

        for player_id, tasks in response.json().items():
            player = next(p for p in players if p.id == player_id)
            yield player, Viewer.tasks_to_text(tasks)

    @staticmethod
    def tasks_to_text(tasks):
        return '; '.join(f'{task["_id"]["$oid"]}: {task["time_left"]}s' for task in tasks)

    def viewer_loop(self):
        loop_count = 0
        while True:
            loop_count += 1

            print(f'Loop #{loop_count} started')

            t_start = time.time()

            players_in_view = random.choices(self.players, k=random.randint(0, self.FOV[0] * self.FOV[1]))
            for player, tasks in self.get_tasks_bulk(players_in_view):
                print(f'    {player.name} tasks: {tasks}')

            print(f'===  Tasks retrieved in {time.time() - t_start}s')

            time.sleep(random.randint(*self.DELAY))


def create_players():
    while True:
        try:
            response = requests.get(f'{BASE_URL}/players/')
            break
        except requests.ConnectionError:
            print('Waiting for server...')
            time.sleep(5)

    players_data = response.json()

    players = []
    for player_data in players_data:
        players.append(Player(id_=player_data["_id"]["$oid"], name=player_data['name']))

    return players


if __name__ == '__main__':
    players = create_players()
    viewer = Viewer(players)

    viewer.viewer_loop()
