import random
import time
from multiprocessing import Process

import requests

BASE_URL = 'http://127.0.0.1:8000'


class Player(object):
    FOV = (32, 32)
    DELAY = (0, 5)

    def __init__(self, id_, name):
        self.id = id_
        self.name = name

    def __create_task(self):
        task_data = {
            'task_time': random.uniform(10, 10 * 60)
        }
        response = requests.post(f'{BASE_URL}/player/{self.id}/create_task/', json=task_data)

        if response.status_code != 201:
            return None

        return response.json()

    def get_tasks(self):
        response = requests.get(f'{BASE_URL}/player/{self.id}/')
        response.raise_for_status()

        return response.json()

    def get_tasks_bulk(self, players):
        response = requests.post(f'{BASE_URL}/players/tasks/', json={'players': [p.id for p in players]})

        for player_id, tasks in response.json().items():
            player = next(p for p in players if p.id == player_id)
            yield player, Player.tasks_to_text(tasks)

    @staticmethod
    def tasks_to_text(tasks):
        return '; '.join(f'{task["_id"]["$oid"]}: {task["time_left"]}s' for task in tasks)

    def player_loop(self, players):
        loop_count = 0
        while True:
            loop_count += 1

            print(f'{self.name} loop #{loop_count} started')
            loop_log = f'{self.name} loop #{loop_count} log:\n'

            t_start = time.time()
            for i in range(random.randint(1, 2)):
                created_task = self.__create_task()
                if created_task:
                    loop_log += f'    Created task {created_task["_id"]["$oid"]} for {created_task["task_time"]}s\n'

            loop_log += f'==  Tasks created in {time.time() - t_start}s\n'

            t_fetch_start = time.time()
            players_in_view = random.choices(players, k=random.randint(0, self.FOV[0] * self.FOV[1]))
            for player, tasks in self.get_tasks_bulk(players_in_view):
                loop_log += f'    {player.name} tasks: {tasks}\n'

            loop_log += f'==  Tasks retrieved in {time.time() - t_fetch_start}s\n'

            loop_log += f'=== Loop took {time.time() - t_start}\n'
            print(loop_log)

            time.sleep(random.randint(*self.DELAY))


def create_players():
    while True:
        try:
            response = requests.get(f'{BASE_URL}/players/')
            break
        except requests.ConnectionError:
            print('Waiting for server...')
            time.sleep(5)

    players_data = response.json()

    players = []
    for player_data in players_data:
        players.append(Player(id_=player_data["_id"]["$oid"], name=player_data['name']))

    return players


if __name__ == '__main__':
    players = create_players()

    for player in players:
        player_process = Process(target=player.player_loop, args=(players,))
        player_process.start()
