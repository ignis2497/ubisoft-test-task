import argparse

from aiohttp import web

from core import LOOP
from core.app import APP
from players.data_generators import prepare_fake_data, clear_db

parser = argparse.ArgumentParser(description='Process some integers.')

parser.add_argument('host_port', metavar='HOST:PORT', type=str, nargs='?', default='127.0.0.1:8000',
                    help='host:port to run server on e.g. 127.0.0.1:8000')
parser.add_argument('--regenerate_db', dest='regenerate', action='store_true',
                    help='clear database and create new random players with tasks')


if __name__ == '__main__':
    args = parser.parse_args()
    host, port = args.host_port.split(':')
    port = int(port)

    if args.regenerate:
        LOOP.run_until_complete(clear_db())

    LOOP.run_until_complete(prepare_fake_data())
    web.run_app(APP, host=host, port=port)
